FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine AS build
WORKDIR /source
 
COPY App/*.csproj ./App/
 
RUN dotnet restore ./App/
  
COPY App/. ./App/
 
WORKDIR /source/App
 
ARG CONFIGURATION
RUN dotnet publish -c $CONFIGURATION  -o /out --no-restore
 
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-alpine
 
WORKDIR /out
COPY --from=build /out ./
  
ENTRYPOINT ["dotnet", "App.dll"]
EXPOSE 80