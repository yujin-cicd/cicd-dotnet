using System.IO;
using ClosedXML.Excel;
using Microsoft.AspNetCore.Mvc;

namespace App.Controllers
{
    [Route("{controller}")]
    [ApiController]
    public class ExcelsController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            var ms = new MemoryStream();
            using var wb = new XLWorkbook();
            var ws = wb.AddWorksheet("sheet1");

            ws.Cell(1, 1).Value = "hi";
            wb.SaveAs(ms);
            
            return File(ms.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "test.xlsx");
        }
    }
}