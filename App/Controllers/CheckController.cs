using System.Collections.Generic;
using App.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace App.Controllers
{
	[Route("{controller}")]
	[ApiController]
	public class CheckController : ControllerBase
	{
		[HttpGet]
		public IActionResult Get()
		{
			var value = ConfigHelper.configuration.GetValue<string>("Host");
			return Ok(value);
		}
	}
}
