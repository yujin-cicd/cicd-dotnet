using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;

namespace App.Controllers
{
    [Route("{controller}")]
    [ApiController]
    public class MondayController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(DateTime.Now);
        }

        [Route("hi")]
        [HttpGet]
        public IActionResult SayHi()
        {
            return Ok("HI!!");
        }
    }
}