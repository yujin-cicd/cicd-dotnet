namespace App.Helpers
{
	using System;
	using System.IO;
	using Microsoft.Extensions.Configuration;
	public static class ConfigHelper
	{
		public static IConfiguration configuration { get; }
			= new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory())
										.AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json",
													 true, true)
										.Build();
	}
}
